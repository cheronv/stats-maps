1. The .gitlab-ci.yaml install dependencies listed in requirements.txt
2. The .gitlab-ci.yaml calls test_map.py
    1. main.py creates some data  and a Bokeh plot.
    2. the Bokeh plot is rendered into an HTML in public/index.html
    3. the rendered Bokeh plot is saved as an image in public/plot.png
3. Gitlab host the content public to the address https://cheronv.gitlab.io/stats-maps/
4. The plot can be integrated into an other webpage with :

 <iframe height="100%" src="https://pums974.gitlab.io/archer-publications/" style="border:0" width="100%">
                <!--In case the browser does not support iframe-->
                <img alt="Test Map"
                     src="https://cheronv.gitlab.io/stats-maps/plot.png"
                     style="width: 90%; display: block; margin-left: auto; margin-right: auto;"/>
</iframe>

import pandas as pd
from bokeh.plotting import figure, show, output_notebook, output_file
from bokeh.themes import built_in_themes
from bokeh.io import curdoc
from bokeh.models import Label

output_file("public/data.html", title='test')

def data():
    df = pd.DataFrame({'time': list(range(2004, 2021)),
                       'papers': [0,1,1,2,0,1,1,1,2,3,1,2,2,5,2,3,5],
                       'thesis': [1,0,0,1,0,0,2,0,1,1,0,1,0,0,0,1,0],
                  'conferences': [0,0,0,0,0,0,2,3,3,4,3,2,5,6,8,15,1],
                       'others': [0,1,2,0,0,0,1,0,0,0,0,0,0,0,0,1,1]})
    df = df.set_index("time")
    df = df[["conferences", "papers", "thesis", "others"][::-1]]
    
#     df["all"] = df.sum(axis=1)
    tcum = df.cumsum(axis=0).add_suffix("_tcum")
    cum = df.cumsum(axis=0).cumsum(axis=1).add_suffix("_cum")
    df = df.assign(**{c: tcum[c] for c in tcum}, **{c: cum[c] for c in cum})
    return df
    
if __name__ == "__main__":
    df = data()
    
    TOOLTIPS = [
        ("Year", "@time"),
        ("type", "$name"),
        ("total", "@$name"),
    ]
    
    curdoc().theme = 'dark_minimal'
    p = figure(plot_width=600, plot_height=400, title="Publications",
               x_range=(2003, 2026), y_range=(0, df.max().max()*1.1),
               toolbar_location="above", tooltips=TOOLTIPS,
               tools="pan,box_zoom,tap,wheel_zoom,reset",
               active_drag="box_zoom", active_tap="tap", active_scroll="wheel_zoom")
    
    names = [str(col) for col in df.columns if not col.endswith("cum")]
    colors = [
    #     '#ffffff',  # white
        '#08F7FE',  # teal/cyan
        '#FE53BB',  # pink
        '#F5D300',  # yellow
        '#00ff41',  # matrix green
    ][::-1]
    
    p.varea_stack(source=df, x='time',
                  stackers=[f"{name}_tcum" for name in names],
                  legend_label=names,
                  color=colors, alpha=0.2)
    
    # Redraw the data with low alpha and slighty increased linewidth:
    n_shades = 10
    diff_linewidth = 1.1
    diff_radius = 0.02
    alpha_values = [1,] + [0.3 / n_shades] * n_shades
    
    for column, color in zip(names, colors):
        for n, alpha in enumerate(alpha_values):
            p.line(x="time", y=f"{column}_cum", source=df,
                   legend_label=column,
                   color=color, alpha=alpha, line_width=1.2 + n * diff_linewidth, name=column)
            p.circle(x="time", y=f"{column}_cum", source=df,
                     legend_label=column,
                     color=color, alpha=alpha, radius=0.15 + n * diff_radius, name=column)
            
        last_time = df.index[-1]
        last_val = df[f"{column}_tcum"][last_time]
        last_pos = df[f"{column}_cum"][last_time]
    
        labels = Label(text=f"{last_val} {column}",
                       text_color=color, text_font_size="10pt", text_baseline="middle",
                       x=last_time + 0.5, y=last_pos)
        p.add_layout(labels)
        
    # reverse the legend entries to match the stacked order
    p.legend.items.reverse()
    p.legend.location = "top_left"
    p.legend.visible = False
    
    #Display plot inline in Jupyter notebook
    # output_notebook()
    
    #Display plot
    show(p)
